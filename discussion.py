# [SECTION 1] Comments
	# comments in Python are done using "ctrl+/" or symbol #


# [SECTION 2] Python Syntax
	# Hello World in Python

print("Hello World!")

	# python discussion.py
	# to call type "python title.py" in the terminal


# [SECTION 3] Indentation
	# Indentation is !IMPORTANT
	# Used to indicate a block of code
	# No need to end statements with semicolon


# [SECTION 4] Variables
	# Variables are containers of data
	# In Python, a variable is declared


age = 35
middle_initial = "C"


# [SECTION 5] Naming Convention
	# we use snake casing in Python unlike JS that use
	# the terminology used for variable names is identifier
	# identifiers/variables should start with a letter (A-Z, a-z), dollar sign($) or an underscore
	# after the first character, identifier can have any combination of characters
		# PEP(Python Enhancement Proposal) https://www.python.org/about/
	# identifiers are Case-Sensitive



# Python allows assigning of values to multiple variables in one line

name1,name2,name3,name4 = "John","Paul", "George", "Ringo"

print(name4)


# [SECTION 6] Data Types
	# Convey what kind of information a variable holds.
	# Commonly used data types:
		# String - Alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"
		# Numbers(int(whole number),float(decimal),complex(algebraic w/ variables))
# INT
num_of_days = 365 
# FLOAT
pi_approx = 3.1416
# COMPLEX
complex_num = 1 + 5j

# to check TYPE OF data type
print(type(num_of_days))

		# Boolean(bool) - for truth values
isLearning = True
isDifficult = True



# [Section 7] Using Variables
# Just like JS variables, are used by simply calling the name of the identifier

print("My name is " + secret_code +" "+ full_name)


# [SECTION 7] Terminal Outputs
	# In Python, printing in the terminal uses the print() function
	# To use variables, concatenate(+symbol) between strings can be used
	# However, we cannot concatenate string to a number or to different data type


		# print("My name age is " + age)
		# Type coercion issue.(string + number)


# [SECTION 8]TypeCasting. 
	# Convertion of data type to another data type



	# Functions that can be used in typecasting
		#1 int() - Integer
		#2 float() - Decimal
		#3 str() - String

print(int(3.75))
print(float(5))
print("My name age is " + str(age))

# Another way to avoid type error in printing without the use of typecasting
# f-strings
print(f"Hi my name is {full_name} and my age is {age}")



# [SECTION 9] Operations
# Operator families to manipulate variables
	# Arithmetic Operators(+-*/**%) - performs mathematical operations
print(1+10) #Addition
print(15-8)
print(18*9)
print(21/7)
print(18%4)
print(2**6)

	# Assignment Operators - used to assign to variables
	#Other Operators -=, *=, /=, %=
num1 = 4

num1+=3 #7
print(num1)

	# Comparison Operators - used compare values (boolean values)
	# Other Operators !=, >=,<=,>,<,==

print(type(1==1)) #False
print(1=="1") #True

	# Logical Operators - used to combine conditional statements
	# in JS || &&
	# OR
print(True or False)
	# AND
print(True and False)
	# NOT
print(not False)

	# NOTE: Search for Hierarchy of Operators when using multiple logical operators ex. (True or False and True)










